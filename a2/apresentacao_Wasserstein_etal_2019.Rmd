---
title: "Apresentação de artigo"
author: Ananda, André, Dennis e Thays
date: "`r Sys.Date()`"
output: 
##  powerpoint_presentation:
  ioslides_presentation:
    widescreen: true
---

## { .vcenter }

<div class="centered">
![](./img/pvalues.png)
</div>

## { .vcenter }

<div class="centered">
![](./img/pvalue1.jpg)
</div>

# O artigo

## Moving to a World Beyond "p < 0.05"

Ronald L. Wasserstein, Allen L. Schirm & Nicole A. Lazar

The American Statistician, 20 March 2019, Volume 73, Issue sup1, p. 1--19 (Editorial)

 https://doi.org/10.1080/00031305.2019.1583913


## Estrutura do artigo

1. "Don't" Is Not Enough
2. Don't Say "Statistically Significant"
3. There Are Many Do's
<!--
    1. Accept Uncertainty
    2. Be Thoughtful
        1. Thoughtfulness in the Big Picture
	    2. Thoughtfulness Through Context and Prior Knowledge
	    3. Thoughtful Alternatives and Complements to P-Values
	    4. Thoughtful Communication of Confidence
    3. Be Open
        1. Openness to Transparency and to the Role of Expert Judgment
	    2. Openness in Communication
    4. Be Modest
-->
4. Editorial, Educational and Other Institutional Practices Will Have to Change
5. It Is Going to Take Work, and It Is Going to Take Time
6. Why Will Change Finally Happen Now?
7. Authors' Suggestions
<!--
    1. Getting to a Post "p < 0.005" Era
    2. Interpreting and Using p
    3. Supplement or Replacing p
    4. Adopting More Holistic Approaches
    5. Reforming Institutions: Changing Publication Policies and Statistical Education
-->

## "Não" não é suficiente

Só relembrando:

* Não concluir somente com base no "estatisticamente significativo" (valor-p ultrapassa certo limiar)
* Não acredite que efeito ou associação existe só porque foi estatisticamente significativo
* Não acredite que efeito ou associação não existe só porque não foi estatisticamente significativo
* Não acredite que o valor-p fornece a probabilidade do observado ser ao acaso ou que a hipótese do teste é verdadeira
* Não concluir nada sobre importância prática ou científica baseado somente na significância estatística (ou falta dela)

> Com a "significância estatística" sendo menos utilizada, o pensamento estatístico será mais.

## Não diga "estatisticamente significativo" 
<!-- ou qualquer de suas variantes-->

* Origem com [Edgeworth (1885)](https://www.jstor.org/stable/25163974)
* Críticas desde [Boring (1919)](http://dx.doi.org/10.1037/h0074554)

> O rótulo de significância estatística não significa ou implica que a associação ou efeito seja altamente provável, real, verdadeira ou importante.

* Problema da divulgação seletiva de resultados "merecedores"
* Problema da categorização em geral

> Indo além da significância estatística possibilita a compreensão do real significado da estatística que é "a ciência do aprendizado a partir dos dados e da mensuração, controle e comunicação da incerteza" ([Davidian e Louis, 2012](https://science.sciencemag.org/content/336/6077/12))

## Existe muito o que fazer

Resumo dos 43 artigos sobre o que fazer

> *Você NÃO vai encontrar nesta edição uma solução que majestosamente substitua o papel descomunal que a significância estatística exerce.* A comunidade estatística ainda não convergiu em um simples paradigma para uso da inferência estatística na pesquisa científica - e na verdade pode nunca fazê-lo.

Recomendação resumida:

> **A**ceite a incerteza. "**P**ense a fundo", seja **a**berto e **m**odesto. 

## Existe muito o que fazer | Aceite incerteza

> "Estatística é frequentemente vendida como um tipo de alquimia que transmuta aleatoridade em certeza. Uma lavagem da incerteza que começa com os dados e conclui com o sucesso mensurado pela significância estatística." ([Gelman, 2016](https://doi.org/10.1080/00031305.2016.1154108)) 

- Inclusão de medida de incerteza para cada estimativa pontual
- Fará procurar melhores medidas, delineamentos mais sensitivos e amostras maiores = Aumentar rigor da pesquisa. Ser **m**odesto e mais **p**ensativo.

## Existe muito o que fazer | Pense a fundo

- Pensar a fundo do ponto de vista mais estatístico
- Pesquisadores que pensam mais a fundo:
    + Expressam claramente seus objetivos
    + Reconhecem quando fazem estudos exploratórios ou estudos planejados mais rígidos
    + Investem em obter dados sólidos
    + Consideram mais técnicas analíticas

## Existe muito o que fazer | Pense a fundo no quadro geral

- Maioria das pesquisas científicas é exploratória ([Tong, 2019](https://doi.org/10.1080/00031305.2018.1518264)) <!--Exige flexibilidade e o preço é a validade das inferências estatísticas-->
- 3 questões elaboradas por pesquisadores que pensam a fundo ([Anderson, 2019](https://doi.org/10.1080/00031305.2018.1537889)):
    + Quais as implicações práticas da estimativa?
    + Quão precisa é a estimativa?
    + O modelo está corretamente especificado? (pressuposições compreendidas e válidas? Outras?)
- *"Guinessometrics"* de [Ziliak (2019)](https://doi.org/10.1080/00031305.2018.1514325) <!--Diante do que tem de melhor, qual magnitude de mudança é humanamente e cientificamente significativa-->
- Benefício prático ([Pogrow, 2019](https://doi.org/10.1080/00031305.2018.1549101))
- Pesquisa pensada a fundo prioriza produção de dados sólidos ([Tong, 2019](https://doi.org/10.1080/00031305.2018.1518264)) <!--com planejamento, delineamento e execução do estudo-->
- Novo modelo de publicação científica cega para os resultados ([Locascio, 2019](https://doi.org/10.1080/00031305.2018.1505658)) <!--Novo modelo de publicação que avalia a pesquisa pela importância das perguntas e pelos métodos utilizados para respondê-las ao invés dos resultados-->

## Existe muito o que fazer | Pense a fundo no contexto e conhecimento prévio

- Significância estatística não diz nada sobre importância prática e ignora estudos anteriores
- Pesquisadores devem se perguntar:
    + O que já se sabe e qual a certeza disso?
    + Qual magnitude do efeito é importante na prática?
- Definição cuidadosa do tamanho do efeito cientificamente significativo deve ser feita antes dos dados serem coletados
- <!--Pesquisa pensada a fundo considera evidências prévias,--> Mecanismo de plausibilidade, delinemanto e qualidade dos dados, custos e benefícios no mundo real, inovação do achado e outros sem priorizar medidas puramente estatísticas ([McShane et al., 2019](https://doi.org/10.1080/00031305.2018.1527253))
- Valor-p pode ser usado como heurística ([Krueger & Heck, 2019](https://doi.org/10.1080/00031305.2018.1470033))

## Existe muito o que fazer | Pense a fundo alternativas e complementos ao valor-p

<font size="5">

- Valor-p numa alternativa não nula <!--, como o tamanho de efeito de importância mínima.--> Tranformar em $\text{valor-}s = -\log_2(p)$ [[Amrhein et al (2019)](https://doi.org/10.1080/00031305.2018.1543137) e [Greenland (2019)](https://doi.org/10.1080/00031305.2018.1529625)]
- SGPV -- Valor-p segunda geração ([Blume et al., 2019](https://doi.org/10.1080/00031305.2018.1537893)) <!--Hipótese composta representando uma amplitude de diferenças que podem ser sem consequência (cientificamente ou na prática)-->
- AnCred -- Análise da Credibilidade ([Matthews, 2019](https://doi.org/10.1080/00031305.2018.1543136)) <!--Leva em consideração o intervalo de confiança e fornece a evidência acreditável para efeito diferente de zero-->
- Valor-p em conjunto com FPR -- Risco de Falso Positivo ([Colquhoun, 2019](https://doi.org/10.1080/00031305.2018.1529622)) <!--Necessita especificação da probabilidade do efeito ser real. Utilizar pelo menos 0,5 o maior valor aceitável de pressupor, resultando risco de falso positivo mínimo-->
- BFB -- Limite do fator de Bayes $= -1/(ep\ln(p))$ ([Benjamin & Berger, 2019](https://doi.org/10.1080/00031305.2018.1543135))
- Combinação do valor-p e um tamanho de efeito suficientemente grande (ambos pré-especificados) para falar em "significante" ([Goodman et al., 2019](https://doi.org/10.1080/00031305.2018.1564697))
- Nível de significância em função da tamanho da amostra ([Gannon et al., 2019](https://doi.org/10.1080/00031305.2018.1518268))
- Retorno da teoria da decisão estatística ([Manski, 2019](https://doi.org/10.1080/00031305.2018.1513377) e [Manski & Tetenov, 2019](https://doi.org/10.1080/00031305.2018.1543617))
- Uso de predição ao invés de inferência sobre parâmetros ([Billheimer, 2019](https://doi.org/10.1080/00031305.2018.1518270))

</font>

## Existe muito o que fazer | Pense a fundo na comunicação da confiança

- Intervalos de ~~confiança~~ compatibilidade [[Greenland (2019)](https://doi.org/10.1080/00031305.2018.1529625) e [Amrhein et al. (2019)](https://doi.org/10.1080/00031305.2018.1543137)]
- Destacar a probabilidade de que a hipótese é verdadeira e a distribuição do tamanho do efeito. Controle de qualidade para a ciência ([Hubbard & Carriquiry, 2019](https://doi.org/10.1080/00031305.2018.1543138))

## Seja aberto

- Transparência <!--descrição completa, compartilhamento de dados e códigos e revisão pré-registrada ("cega" para resultados)-->
- Julgamento do especialista
- Comunicação
- Problema do viés da publicação
- Facilitar reprodução e/ou análises alternativas

## Seja modesto

- Explicitar limitações do estudo
- Estatística não é a realidade
- Reprodução incentivada

## Mudanças editoriais, educacionais e outras práticas institucionais que terão que ocorrer

- Foco editoriais
- Educação

## Vai levar tempo e vai dar trabalho

- Explicação sociológica
- Mudança gradual ou proibição total?
- Aceitável:
    1. Controle de qualidade industrial
	2. Seleção de variáveis (**Automatizado**)
	3. Algumas áreas (usando limites bem reduzidos)
	4. Estudos bem planejados, delineados e de caráter confirmatório

<!--## Por que a mudança finalmente acontecerá? -->


## Sugestões dos autores

[Brownstein et al. (2019)](https://doi.org/10.1080/00031305.2018.1529623)

Estatísticos:

- Melhorar habilidades de comunicação
- Compreender suas múltiplas tarefas como colaborador
- Posicionamento de liderança
- Entender o problema
- Promover a participação de colaboradores nas problemas estatísticos
- Manter planejamento estatístico por escrito (e todas mudanças)
- Promover a responsabilidade do grupo na qualidade dos dados, segurança e documentação
- Reduzir modelagem e testes não planejados (*HARK-ing*, *p-hacking*)

<!--
    1. Getting to a Post "p < 0.005" Era
    2. Interpreting and Using p
    3. Supplement or Replacing p
    4. Adopting More Holistic Approaches
    5. Reforming Institutions: Changing Publication Policies and Statistical Education

<!--
# Artigos dessa edição

---

[Amrhein et al. (2019)](https://doi.org/10.1080/00031305.2018.1543137)

[Anderson (2019)](https://doi.org/10.1080/00031305.2018.1537889)

[Benjamin & Berger (2019)](https://doi.org/10.1080/00031305.2018.1543135)

[Billheimer (2019)](https://doi.org/10.1080/00031305.2018.1518270)

[Blume et al. (2019)](https://doi.org/10.1080/00031305.2018.1537893)

[Calin-Jageman & Cumming (2019)](https://doi.org/10.1080/00031305.2018.1518266)

[Colquhoun (2019)](https://doi.org/10.1080/00031305.2018.1529622)

[Gannon et al. (2019)](https://doi.org/10.1080/00031305.2018.1518268)

[Goodman et al. (2019)](https://doi.org/10.1080/00031305.2018.1564697)

[Greenland (2019)](https://doi.org/10.1080/00031305.2018.1529625)

[Hubbard & Carriquiry (2019)](https://doi.org/10.1080/00031305.2018.1543138)

[Krueger & Heck (2019)](https://doi.org/10.1080/00031305.2018.1470033)

[Locascio (2019)](https://doi.org/10.1080/00031305.2018.1505658)

[Manski (2019)](https://doi.org/10.1080/00031305.2018.1513377)

[Manski & Tetenov (2019)](https://doi.org/10.1080/00031305.2018.1543617)

[Matthews (2019)](https://doi.org/10.1080/00031305.2018.1543136)

[McShane et al. (2019)](https://doi.org/10.1080/00031305.2018.1527253)

[O'Hagan, 2019](https://doi.org/10.1080/00031305.2018.1518265)

[Pogrow (2019)](https://doi.org/10.1080/00031305.2018.1549101)

[Tong (2019)](https://doi.org/10.1080/00031305.2018.1518264)

[Ziliak (2019)](https://doi.org/10.1080/00031305.2018.1514325)

-->
