---
title: "Apresentação de artigo e classificação do tipo de pesquisa"
author: André Luiz Grion
date: "`r Sys.Date()`"
output: 
  ioslides_presentation:
    mathjax: MathJax-master/MathJax.js?config=TeX-AMS-MML_HTMLorMML
---

# O artigo

## Comparing implementations of global and local indicators of spatial association

Roger S. Bivand e David W. S. Wong

TEST, Setembro 2018, Volume 27, Issue 3, p. 716--748

https://doi.org/10.1007/s11749-018-0599-x

![Bivand](./img/bivand.jpg){ width=18% } ![Wong](./img/wong.jpg)

## Resumo

Comparação da implementação de medidas de autocorrelação 

- $I$ e $I_i$ de Moran
- $G$ e $G_i$ Getis-Ord
- $C$ de Geary

que estão disponíveis em alguns *softwares*

- Crimestat
- GeoDa
- ArcGIS
- PySAL
- pacotes do R (especialmente **spdep**)

## Objetivo

Comparar as implementações de medidas globais e locais de autocorrelação espacial e estabelecer as razões para qualquer diferença encontrada de modo que os usuários possam ter certeza que sua escolha de *software* não está prejudicando seu trabalho.

## Introdução

![](./img/introduction2.png)

## Introdução

![](./img/introduction3.png)

<!--
tela para print screen: 172,80%
- Heterogeneidade e autocorrelação espacial (volta na conclusão)
- Breve histórico dos *softwares*
- O problema de implementação para reprodução de resultados. Exemplo: Bivand (1992). (com código logo na primeira página o que expressa bem a natureza aplicada da pesquisa e Tabela 1 de resultados)

```{r, eval = FALSE}
##system("sudo apt-get install libgdal-dev libproj-dev") ## necessário para o rgdal
##install.packages("rgdal")
##install.packages("spData")
##system("sudo apt install libudunits2-dev") ## necessário para o units que é necessário para o sf que é necessário para o spdep
##install.packages("spdep")
eire <- rgdal::readOGR(system.file("shapes/eire.shp", package = "spData")[1])
library(spdep)
eire.nb <- poly2nb(eire)
head(eire)
moran.test()
plot(eire)
```
- Vizinhança utilizada em Bivand (1992). Consideração de linha férrea.
- Preocupação dos usuários com diferentes resultados obtidos em diferentes *softwares*. Se os dados e os pesos utilizados são os mesmos o resultado deve ser o mesmo.
-->

<!--
- Ainda comenta sobre o que não será considerado, autocorrelação em variáveis categóricas mas sgere abordá-la em algum momento no futuro.
-->

## Indicadores globais e locais

<!--
- Medidas globais expressam a força da autocorrelação espacial de uma variável quantitativa de interesse em todo banco de dados de áreas. (Possivelmente). A importância da consideração da matriz de pesos espaciais para expressar o processo aleatório subjacente. Exemplo do tabuleiro de xadrez.
- Medidas locais são a decomposição da medida global para cada unidade de área. Será afetado pela falta de consideração de variáveis e/ou processo espacial global.
- Ambas medidas podem detectar má especificação. Detecção de "hotspots" é fortemente afetada pela capacidade de identificar outras formas de má-especificação. Além disso necessita de correção para comparação múltipla.
- Lista as representações padrões de medidas sem cobrir algumas alternativas e medidas mais especializadas como as que levam em consideração a incidência e a população em risco (PROCURAR!).
-->

I de Moran (Moran, 1950)

$$
I = \dfrac{n \sum_{(2)} w_{ij}z_iz_j}{S_0 \sum_{i=1}^n z_i^2}
$$

$$
E(I) = - \dfrac{1}{(n-1)}
$$

$$
E_N(I^2) = \dfrac{n^2S_1-nS_2+3S_0^2}{S_0^2(n^2-1)}
$$

## Indicadores globais e locais

$$
S_0 = \sum_{(2)} w_{ij}
$$

$$
S_1 = \dfrac{1}{2}\sum_{(2)}(w_{ij} + w_{ji})^2
$$

$$
S_2 = \sum_{i=1}^n\left(\sum_{j=1}^n w_{ij} + \sum_{j=1}^n w_{ji}\right)^2
$$

<!--
- Problemas quando existem observações sem vizinhos
- spdep ajusta $n$ para o caso de observações sem vizinhos
- Regressão só com intecepto deve concordar exatamente com o uso de $\bar{x}$ no I de Moran padrão assumindo normalidade. Só `spdep` tem implementado

## Destaque nas implementações

- `spdep` ajusta $n$ para o caso de observações sem vizinhos
- Implementação no `spdep` permite uso de I de Moran em resíduos de regressão.
- Nenhum ajusta para amostras pequenas como considerado em Cliff and Ord (1971)
- Nenhum com teste exato para resíduos da regressão com apresentado por Hepple (1998).
- Teste exato para resíduos de regressão apresentado por Bivand et al. (2009) no `spdep` (também para I de Moran local)
- Aproximação no ponto de sela proposto por Tiefelsdorf (2002) no `spdep`  (também para I de Moran local)
-->

## Implementações nos *softwares*

- Acesso a matriz de pesos e resultados
- Correção para comparação múltipla nas medidas locais


## Dados de teste e localizações

![[Consumer Data Research Centre (CDRC)](https://data.cdrc.ac.uk/tutorial/an-introduction-to-spatial-data-analysis-and-visualisation-in-r)](./img/data.png)

## Resultados testes globais

![](./img/tab3.png)


## Resultados testes globais

![](./img/tab8.png)


## Resultados testes locais

![](./img/fig9.png)


## Resultados testes locais

![](./img/fig10.png)


## Conclusões

- Problemas de comparação múltipla
- Má-especificação global dos dados (**spdep** resolve)
- Diferenças na localização de *hotspots* (método de permutação ou exato)
- Questão da heterogeneidade

## Classificação quanto ao tipo de pesquisa

- Abordagem: Quantitativa
- Natureza: Aplicada
- Objetivos: Descritiva, **Explicativa**
- Procedimentos: Experimental, Análise documental
